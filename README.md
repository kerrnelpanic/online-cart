# README #

This is an implementation of a very simple checkout system. The requirements and some further annotations are defined in UMSETZUNG_WARENKORBBERECHNUNG.md. The requirements are originally meant to be implemented as php-Project, so in this project, some adaptions were made, as necessary, to realize it as restful spring boot app.  

Version 0.1  

### setup ###

Requirements: openjdk-11.0.9  

Clone the repository to a suitable directory on Your System (eg. /home/YourUserHome/checkout-cart). You can copy-paste the command from the gitbucket-page, just click on 'clone', copy the line and paste it to your console in the prepared directory.  
Then, execute `bash mvnw clean install` from within the cloned directory.  Find the *jar* file in the subfolder *target* and run it by executing `java -jar -Dserver.port=%somethingBetween8000and65000% %jarname%.jar`. Change the values between %'s to appropriate ones.
### usage ###

Assuming port is set to 65000, open   
`http://localhost:65000/products`  
`http://localhost:65000/items`  
`http://localhost:65000/vouchers`  
`http://localhost:65000/orders`  
in your browser for an overview.  
`http://localhost:65000`  
will show the example described in *UMSETZUNG_WARENKORBBERECHNUNG.md*.  

### contact ###

Baldur Kellner - baldur.kellner@gmail.com