## initialization ##

* https://start.spring.io/  
  * new maven project  
    * dependencies: Web,JPA,H2
* extract as project folder
* initialize git
  * create empty repo on hub/bit
  * init local repo and do 1st commit
  * configure remote
  * push -u
  
## adding new spring-boot dependencies ##

* declare in pom.xml
* update (Ctrl+Shift+O)  