Funktionale Anforderungen

Ein Nutzer hat einen Warenkorb
Der Warenkorb beinhaltet mehrere Positionen von Produkten
Produkte besitzen eine ID und einen Preis.
Positionen im Warenkorb besitzen einen Bezug zu dem Produkt, einen Preis und eine Quantität wie oft sie enthalten sind
Im Warenkorb können mehrere Rabattcodes eingelöst werden. Ein Rabattcode kann absolut oder prozentual den Wert des Warenkorbs reduzieren und besitzt somit einen Code, einen Typ (absolut, prozentual) und einen Wert. Absolute Rabattcodes werden immer vor den prozentualen Rabattcodes angewendet.

Aufgabe

Es soll ein Script geschrieben werden, welches die Summe eines bestehenden Warenkorb eines Kunden anhand eines vorgebenen Szenarios berechnet. Hierbei dient das Szenario als Programmablauf.
Es sollen die Positionen (Produkt ID, Preis der Position, Menge), eine Zwischensumme, sowie die eingelösten Rabattcodes (Codes und Einfluss auf Zwischensumme) über der Warenkorbsumme aufgelistet werden.

Nichtfunktionale Anforderungen

Die Umsetzung des Warenkorbs soll mit der Programmiersprache PHP erfolgen
Der Warenkorb soll in einfachem HTML dargestellt werden
Steuern und sonstige Einflüsse wie z.B. die Reihenfolge der Berechnungen müssen nicht beachtet werden
Eine Datenbankanbindung muss nicht beachtet werden
In der Umsetzung soll das MVC-Design-Pattern grundlegend enthalten sein
Es soll eine Spezifikation erstellt werden. Die Spezifikation soll den grundlegenden Aufbau des Projekts darstellen und dient als Leitfaden für die Implementierung der Software. Es soll ein UML-Klassendiagramm enthalten sein, an welchem sich die Beziehungen der Klassen zueinander ablesen lassen.

Hinweise

Als Einstieg in das MVC Pattern reicht eine klar strukturierte Datei, welche sowohl als Controller und als View dient, somit beginnt die Initialisierung im Controller und endet auch dort mit der Ausgabe
Verwendete Klassen sind als separate Dateien zu handhaben und werden initial im Controller eingebunden
Beispieldaten können als Array oder Json den jeweiligen Instanzen übergeben werden oder sind bereits dort definiert
Notwendige Methoden für den Ablauf sind aus dem Szenario zu abstrahieren
Methoden und Klassenamen werden auf Englisch definiert
Grundlegendes Errorhandling sollte beachtet werden, muss jedoch nicht gesondert in der HTML Ausgabe beachtet werden
Es wird Wert auf einen guten Stil und die Einhaltung von gängigen Pattern und Programmierparadigmen gelegt

Szenario
Ein Kunde legt in seinen Warenkorb 2 mal das Produkt 1 zu einem Preis von je 13,95 EUR, sowie 1 mal das Produkt 2 zu einem Preis von 34,90 EUR. Außerdem löst er einen %-Rabattcode "springsale" von 10% ein. Zusätzlich dazu noch einen Willkommens-Rabattcode "newsletterwelcome" im Wert von 5 EUR.
Welche Endsumme hat sein Warenkorb?