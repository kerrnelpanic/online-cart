package de.fonscodice.checkout.checkoutcart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class CheckoutCartApplicationTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void servletContext_productControllerProvided() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        Assert.notNull(servletContext, "ServletContext not loaded");
        Assert.isTrue(servletContext instanceof MockServletContext, "servletContext is not a MockServletContext");
        Assert.notNull(webApplicationContext.getBean("productController"), "bean not available");
    }

    @Test
    public void forwardToIndexHtml() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print())
                .andExpect(view().name("forward:index.html"));
    }

    @Test
    public void singleProduct() throws Exception {
        this.mockMvc.perform(get("/products/{id}", 1)).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$.name").value("Produkt1"));
    }
    @Test
    public void allProducts() throws Exception {
        this.mockMvc.perform(get("/products/")).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.productList").exists());
    }

    @Test
    public void singleItem() throws Exception {
        this.mockMvc.perform(get("/items/{id}", 3)).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$.product").exists());
    }
    @Test
    public void allItems() throws Exception {
        this.mockMvc.perform(get("/items/")).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.itemList").exists());
    }

    @Test
    public void singleVoucher() throws Exception {
        this.mockMvc.perform(get("/vouchers/{id}", 5)).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$.code").value("newsletterwelcome"));
    }
    @Test
    public void allVouchers() throws Exception {
        this.mockMvc.perform(get("/vouchers/")).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.voucherList").exists());
    }

    @Test
    public void singleOrder() throws Exception {
        this.mockMvc.perform(get("/orders/{id}", 7)).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$.items").exists())
                .andExpect(jsonPath("$.vouchers").exists());
    }
    @Test
    public void allOrders() throws Exception {
        this.mockMvc.perform(get("/orders/")).andDo(print())
                .andExpect(status().isOk())

                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.orderList").exists());
    }
}
