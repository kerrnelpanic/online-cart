package de.fonscodice.checkout.checkoutcart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"de.fonscodice"})
public class CheckoutCartApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckoutCartApplication.class, args);
	}

}
