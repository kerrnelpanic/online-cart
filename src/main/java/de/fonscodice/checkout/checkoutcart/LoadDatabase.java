package de.fonscodice.checkout.checkoutcart;

import de.fonscodice.checkout.checkoutcart.item.Item;
import de.fonscodice.checkout.checkoutcart.item.ItemRepository;
import de.fonscodice.checkout.checkoutcart.order.Order;
import de.fonscodice.checkout.checkoutcart.order.OrderRepository;
import de.fonscodice.checkout.checkoutcart.product.Product;
import de.fonscodice.checkout.checkoutcart.product.ProductRepository;
import de.fonscodice.checkout.checkoutcart.voucher.Voucher;
import de.fonscodice.checkout.checkoutcart.voucher.VoucherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(ProductRepository pRep, ItemRepository iRep, VoucherRepository vRep, OrderRepository oRep) {
        Product p1 = new Product("Produkt1", 13.95);
        Product p2 = new Product("Produkt2", 34.90);
        Item i1 = new Item(p1, 2);
        Item i2 = new Item(p2, 1);
        Order o = new Order();
        Set<Item> sItem = new HashSet<>();
        sItem.add(i1);
        sItem.add(i2);
        o.setItems(sItem);
        Voucher v1 = new Voucher("newsletterwelcome", Voucher.VOUCHER_TYPE.ABSOLUTE,5);
        Voucher v2 = new Voucher("springsale", Voucher.VOUCHER_TYPE.RELATIVE,10);
        Set<Voucher> sVoucher = new HashSet<>();
        sVoucher.add(v1);
        sVoucher.add(v2);
        o.setVouchers(sVoucher);
        o.getCartTotal();

        return args -> {
            log.info("Preloading " + pRep.save(p1));
            log.info("Preloading " + pRep.save(p2));
            log.info("Preloading " + iRep.save(i1));
            log.info("Preloading " + iRep.save(i2));
            log.info("Preloading " + vRep.save(v1));
            log.info("Preloading " + vRep.save(v2));
            log.info("Preloading " + oRep.save(o));
        };
    }

}
