package de.fonscodice.checkout.checkoutcart.order;

import de.fonscodice.checkout.checkoutcart.item.Item;
import de.fonscodice.checkout.checkoutcart.voucher.Voucher;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "CUSTOMER_ORDER")
public class Order {
    private @Id
    @GeneratedValue
    long id;
    double total;

    @ElementCollection
    private Set<Item> items;
    @ElementCollection
//    @OneToMany(cascade = CascadeType.ALL)  // needed, when vouchers not preloaded
    private Set<Voucher> vouchers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Set<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(Set<Voucher> vouchers) {
        this.vouchers = vouchers;
    }

    public double getSubTotal() {
        return items.stream()
                .mapToDouble(Item::getSubTotal)
                .sum();
    }

    private void applyVouchers() {
        vouchers.stream()
                .sorted()
                .forEach(voucher -> total -= voucher.apply(total)); // relies on side effect
    }

    public double getCartTotal() {
        total = getSubTotal();
        applyVouchers();
        return total;
    }

    @Override
    public String toString() {
        return "Order{" +
                ", total=" + String.format("%.2s", total) +
                ", items=" + items +
                ", vouchers=" + vouchers +
                '}';
    }

}
