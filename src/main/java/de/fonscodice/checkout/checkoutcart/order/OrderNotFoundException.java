package de.fonscodice.checkout.checkoutcart.order;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(Long id) {
        super("Order expired or missing " + id);
    }
}
