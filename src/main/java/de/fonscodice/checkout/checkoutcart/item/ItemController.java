package de.fonscodice.checkout.checkoutcart.item;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ItemController {
    private final ItemRepository repository;

    public ItemController(ItemRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/items")
    CollectionModel<EntityModel<Item>> all() {
        List<EntityModel<Item>> items = repository.findAll().stream()
                .map(item -> EntityModel.of(item,
                        linkTo(methodOn(ItemController.class).one(item.getId())).withSelfRel(),
                        linkTo(methodOn(ItemController.class).all()).withRel("items")))
                .collect(Collectors.toList());

        return CollectionModel.of(items,
                linkTo(methodOn(ItemController.class).all()).withSelfRel());
    }

    @GetMapping("/items/{id}")
    EntityModel<Item> one(@PathVariable Long id) {
        Item item = repository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException(id));

        return EntityModel.of(item,
                linkTo(methodOn(ItemController.class).one(id)).withSelfRel(),
                linkTo(methodOn(ItemController.class).all()).withRel("items"));
    }

}
