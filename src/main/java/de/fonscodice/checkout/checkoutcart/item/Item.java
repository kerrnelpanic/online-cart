package de.fonscodice.checkout.checkoutcart.item;

import de.fonscodice.checkout.checkoutcart.product.Product;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Item {
    private @Id
    @GeneratedValue
    long id;
    @OneToOne
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_item_product"))
    private Product product;
    private int count;

    public Item() {
    }

    public Item(Product product, int count) {
        this.product = product;
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getSubTotal() {
        return product.getPrice() * count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(product, item.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", product=" + product +
                ", count=" + count +
                '}';
    }
}
