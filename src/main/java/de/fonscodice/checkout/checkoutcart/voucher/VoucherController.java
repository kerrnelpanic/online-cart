package de.fonscodice.checkout.checkoutcart.voucher;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class VoucherController {
    private final VoucherRepository repository;

    public VoucherController(VoucherRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/vouchers")
    CollectionModel<EntityModel<Voucher>> all() {
        List<EntityModel<Voucher>> vouchers = repository.findAll().stream()
                .map(voucher -> EntityModel.of(voucher,
                        linkTo(methodOn(VoucherController.class).one(voucher.getId())).withSelfRel(),
                        linkTo(methodOn(VoucherController.class).all()).withRel("vouchers")))
                .collect(Collectors.toList());

        return CollectionModel.of(vouchers,
                linkTo(methodOn(VoucherController.class).all()).withSelfRel());
    }

    @GetMapping("/vouchers/{id}")
    EntityModel<Voucher> one(@PathVariable Long id) {
        Voucher voucher = repository.findById(id)
                .orElseThrow(() -> new VoucherNotFoundException(id));

        return EntityModel.of(voucher,
                linkTo(methodOn(VoucherController.class).one(id)).withSelfRel(),
                linkTo(methodOn(VoucherController.class).all()).withRel("vouchers"));
    }
}
