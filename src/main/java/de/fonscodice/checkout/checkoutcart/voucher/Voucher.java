package de.fonscodice.checkout.checkoutcart.voucher;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Voucher implements Comparable<Voucher> {
    private @Id
    @GeneratedValue
    long id;

    public enum VOUCHER_TYPE {
        ABSOLUTE,
        RELATIVE;
    }

    private String code;
    private VOUCHER_TYPE voucherType;
    private double value;
    private double effect;

    public Voucher() {

    }

    public Voucher(String code, VOUCHER_TYPE voucherType, double value) {
        this.code = code;
        this.voucherType = voucherType;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public VOUCHER_TYPE getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VOUCHER_TYPE voucherType) {
        this.voucherType = voucherType;
    }

    public double getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public double getEffect() {
        return effect;
    }

    public double apply(double undiscounted) {
        return effect = undiscounted
                - (voucherType == VOUCHER_TYPE.ABSOLUTE ? undiscounted - value : undiscounted * (1. - value / 100.));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher = (Voucher) o;
        return value == voucher.value &&
                Objects.equals(code, voucher.code) &&
                voucherType == voucher.voucherType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, voucherType, value);
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "code='" + code + '\'' +
                ", voucherType=" + voucherType +
                ", value=" + String.format("%.2f", value )+
                '}';
    }

    @Override
    public int compareTo(Voucher o) {
        return voucherType.compareTo(o.voucherType);
    }
}
