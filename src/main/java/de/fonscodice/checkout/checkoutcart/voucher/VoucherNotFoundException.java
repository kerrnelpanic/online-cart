package de.fonscodice.checkout.checkoutcart.voucher;

public class VoucherNotFoundException extends RuntimeException {
    public VoucherNotFoundException(Long id) {
        super("Could not find voucher " + id);
    }
}
